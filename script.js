/*     
Прототипное насследование в JS: насследование объектом свойств и методов принадлежащих его объекту прототипу. Таким образом в JS можно организовать объекты в цепочку таким образом, что в случае если нужное свойство не будет найдено в самом объекте то оно  может быть найдено в объекте на который есть ссылка что это его прототип, если не будет в этом объекте то поиск будет произведен далее по ссылке на прототип прототипа. 
*/

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name.toUpperCase();
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }
  set name(newName) {
    this._name = newName;
  }
  set age(newAge) {
    this._age = newAge;
  }
  set salary(newSalary) {
    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }
  get lang() {
    return this._lang.join(", ");
  }
  set salary(newSalary) {
    this._salary = newSalary;
  }
  set lang(newLang) {
    this._lang.push(newLang);
  }
}

const diego = new Programmer(
  (name = "Diego"),
  (age = "30"),
  (salary = "2000"),
  (lang = ["ukrainian", "russian", "spain"])
);
showData.call(diego);

diego.lang = "france";
diego.salary = "2500";
showData.call(diego);

const william = new Programmer(
  (name = "William"),
  (age = "34"),
  (salary = "1800"),
  (lang = ["english, german"])
);
showData.call(william);

const abdul = new Programmer(
  (name = "abdul"),
  (age = "25"),
  (salary = "1500"),
  (lang = ["english, arabic"])
);
showData.call(abdul);

function showData() {
  console.group("Programmer");
  console.log("Name: ", this.name);
  console.log("Age: ", this.age);
  console.log("Salary: ", this.salary, "USD");
  console.log("Language: ", this.lang);
}
